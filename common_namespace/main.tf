variable "namespace_operators" {
  default = "operators"
}

variable "namespace_monitoring" {
  default = "cluster-monitoring"
}

resource "kubernetes_namespace" "operators" {
  metadata {
    name = var.namespace_operators
  }
}

output "namespace_operators" {
  value = kubernetes_namespace.operators.metadata[0].name
}

variable "namespace_ingress" {
  default = "ingress-nginx"
}

resource "kubernetes_namespace" "ingress" {
  metadata {
    name = var.namespace_ingress
  }
}

output "namespace_ingress" {
  value = kubernetes_namespace.ingress.metadata[0].name
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = var.namespace_monitoring
  }
}

output "namespace_monitoring" {
  value = kubernetes_namespace.monitoring.metadata[0].name
}

